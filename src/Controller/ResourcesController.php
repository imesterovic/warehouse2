<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Resources Controller
 *
 * @property \App\Model\Table\ResourcesTable $Resources
 */
class ResourcesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Locations', 'Managers'],
            'sortWhitelist' => ['id', 'name', 'Locations.longitude', 'Locations.latitude', 'Managers.name', 'created', 'modified']
        ];
        $resources = $this->paginate($this->Resources);

        return $this->_jsonResponse($resources->toArray());
    }

    /**
     * View method
     *
     * @param string|null $id Resource id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $resource = $this->Resources->get($id, [
            'contain' => ['Locations', 'Managers']
        ]);

        return $this->_jsonResponse($resource->toArray());
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $resource = $this->Resources->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if(!isset($data['location']['latitude']) && !isset($data['location']['longitude']))
            {
                $data['location']['latitude'] = '';
                $data['location']['longitude'] = '';
            }
            $resource = $this->Resources->patchEntity($resource, $data);
            if ($this->Resources->save($resource)) {
                return $this->_jsonResponse($resource->toArray(), false, __('The resource has been saved.'));
            }
            return $this->_jsonResponse($resource->toArray(), true,
                __('The resource could not be saved. Please, try again.'),
                $resource->errors());
        }

        return $this->_jsonResponse([]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Resource id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $resource = $this->Resources->get($id, [
            'contain' => ['Locations', 'Managers']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if(!isset($data['location']['latitude']) && !isset($data['location']['longitude']))
            {
                $data['location']['latitude'] = '';
                $data['location']['longitude'] = '';
            }
            $resource = $this->Resources->patchEntity($resource, $data);
            if ($this->Resources->save($resource)) {
                return $this->_jsonResponse($resource->toArray(), false, __('The resource has been saved.'));
            }
            return $this->_jsonResponse($resource->toArray(), true,
                __('The resource could not be saved. Please, try again.'),
                $resource->errors());
        }
        return $this->_jsonResponse([]);
    }

    /**
     * Delete method
     *
     * @param string|null $id Resource id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $resource = $this->Resources->get($id);
        if ($this->Resources->delete($resource)) {
            return $this->_jsonResponse($resource->toArray(), false, __('The resource has been deleted.'));
        }
        return $this->_jsonResponse($resource->toArray(), true,
            __('The resource could not be deleted. Please, try again.'),
            $resource->errors());
        return $this->_jsonResponse([]);
    }
}
