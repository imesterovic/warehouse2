## CakePHP warehouse API - with ORM

## Installation

1. git clone https://imesterovic@bitbucket.org/imesterovic/warehouse2.git
2. composer update
3. Add the schema to a new database. /config/schema/warehouse.sql
4. Configure your database credentials in /config/app.php. Make sure to use the same database name as in step 3.